<?php

namespace fighcell\sms;

use fighcell\sms\base\BaseSms;
use yii\base\InvalidConfigException;

/**
 * Class AbstractSms
 * @package fighcell\sms
 */
abstract class AbstractSms extends BaseSms
{
    /**
     * @return mixed
     */
    abstract public function getClient();

    /**
     * @return bool
     */
    abstract public function checkCredentials();

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->useFileTransport === false) {
            if (!$this->checkCredentials()) {
                throw new InvalidConfigException(self::class . ": Credentials are required!");
            }
        }

        parent::init();
    }
}
